#include <tuple>
#include <iostream>
#include <array>
#include <utility>
#include <vector>

struct Box {

    int i_;
    float f_;

};

template <typename T>
T convert(Box b);

template <>
int convert(Box b) {
    return b.i_;
}

template <>
float convert(Box b) {
    return b.f_;
}

template <typename T> 
Box box(T val) {}

template <>
Box box(float f) {
    Box b{};
    b.f_ = f;
    return b;
}

template <>
Box box(int i) {
    Box b{};
    b.i_ = i;
    return b;
}

template <typename R, typename ...Args>
struct function_pointer {

    function_pointer(R(*ptr)(Args...)) : ptr_(ptr) {
    }
    
    R(*ptr_)(Args...);
    
    using return_type = R;
    using argument_types = std::tuple<Args...>;

    template <typename ...Boxes, typename Indices = std::make_index_sequence<sizeof...(Boxes)>>
    return_type call(Boxes... boxes) {
        return inner_call(Indices{}, boxes...);
    }

    template <typename Indices = std::make_index_sequence<sizeof...(Args)>>
    return_type call(std::vector<Box> boxes) {
        return inner_call_vector(Indices{}, boxes);
    }

    private:
        template<typename... Boxes, std::size_t... I>
        return_type inner_call(std::index_sequence<I...>, Boxes... boxes)
        {
            ptr_(convert<typename std::tuple_element<I, argument_types>::type>(boxes)...);      
        }

        template<typename... Boxes, std::size_t... I>
        return_type inner_call_vector(std::index_sequence<I...>, std::vector<Box> boxes)
        {
            ptr_(convert<typename std::tuple_element<I, argument_types>::type>(boxes[I])...);      
        }

        // template<std::size_t... I>
        // return_type assign(std::index_sequence<I...>, std::vector<Box> boxes, Args... args)
        // {
        //     ptr_(convert<typename std::tuple_element<I, argument_types>::type>(boxes[I])...);      
        // }

};

template<class none = void>
constexpr void f(std::vector<Box>& v)
{
    return;
}

template<typename T, typename... Args>
void f(std::vector<Box>& v, T first, Args... rest)
{
    v.push_back(box(first));
    f(v, rest...);
    
}


template <typename R, typename ...Args>
function_pointer<R, Args...> infer(R(*ptr)(Args...)) {
    return function_pointer<R, Args...> (ptr);
}


void dummy(float a, int b) {
    std::cout << "a = " << a << ", b" << b;
}

int main() {

    auto fp = infer(&dummy);
    Box a;
    Box b;
    a.f_ = 7.5f;
    b.i_ = 5;
    fp.call(a, b);
    fp.call(std::vector<Box>({a, b}));
}