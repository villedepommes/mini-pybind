#include <tuple>
#include <iostream>
#include <utility>
#include <functional>
#include <Python.h>

template <typename T>
T unbox(PyObject* po);

template <>
long unbox(PyObject* po) {
    return PyLong_AsLong(po);
}

template <>
double unbox(PyObject* po) {
    return PyFloat_AsDouble(po);
}

template <typename T> 
PyObject* box(T val);

template <>
PyObject* box(double f) {
    return PyFloat_FromDouble(f);
}

template <>
PyObject* box(long i) {
    return PyLong_FromLong(i);
}

// how to extract argument and return types
// https://stackoverflow.com/questions/28033251/can-you-extract-types-from-template-parameter-function-signature
template <typename R, typename ...Args>
struct function_pointer {

    function_pointer(R(*ptr)(Args...)) : ptr_(ptr) {
    }
    
    R(*ptr_)(Args...);
    
    // stash input types
    using return_type = R;
    using argument_types = std::tuple<Args...>;

    // https://en.cppreference.com/w/cpp/utility/integer_sequence
    template <typename Indices = std::make_index_sequence<sizeof...(Args)>>
    PyObject* call(PyObject* tup) {
        return call_(Indices{}, tup);
    }

    private:
        template<std::size_t... I>
        PyObject* call_(std::index_sequence<I...>, PyObject* tup)
        {
            //this unboxes tuple's I-th item using unbox<T> defined above
            // into this type: std::tuple_element<I, argument_types>::type>
            //unbox<typename std::tuple_element<I, argument_types>::type>(PyTuple_GetItem(tup, I)

            // the result is boxed back into PyObject*
            return box(ptr_(unbox<typename std::tuple_element<I, argument_types>::type>(PyTuple_GetItem(tup, I))...));
        }
};

// a helper to instantiate a function_pointer that infers
// template arguments automatically and allows a caller to store
// the result in auto compared to declaring a variable with
// all the template arguments manually
template <typename R, typename ...Args>
function_pointer<R, Args...> infer(R(*ptr)(Args...)) {
    return function_pointer<R, Args...> (ptr);
}

static long add(long a, long b) {
    return a + b;
}

// This class allows one to hide a hairy templated class into a **non-templated** class
// Inside lambda's body we can use `auto` to hide template arguments.
class Capsule {

public:
    template <typename R, typename ...Args>
    Capsule(R(*ptr)(Args...)) {
        wrapper_ = [ptr](PyObject* self, PyObject* args) -> PyObject* {
            auto fp = infer(ptr);
            return fp.call(args);
        };
    }

    PyObject* operator()(PyObject* self, PyObject* args) {
        return wrapper_(self, args);
    }

private:
    std::function<PyObject*(PyObject*, PyObject*)> wrapper_;
};

extern "C" {
    static PyObject* add_wrapper(PyObject* self, PyObject* args) {
        
        // this is another way to store a function_pointer in a non-templated variable
        //auto fp = infer(add);
        //return fp.call(args);

        // wrap in a capsule without the template stuff
        static Capsule cap(add);
        return cap(self, args);
    }
}

static PyMethodDef methods[] = {
    {"add", add_wrapper, METH_VARARGS, ""},
    {NULL}
};

static PyModuleDef minipybymodule = {
    PyModuleDef_HEAD_INIT,
    "minipybind",
    "A mini-pybind extension",
    -1,
    methods,
};

static PyObject* MiniPyByError;

// PyInit should have the same name as Extension()
// in setup.py and in import statements
PyMODINIT_FUNC PyInit_minipyby() {
    PyObject* module;

    module = PyModule_Create(&minipybymodule);
    if (module == NULL) {
        return NULL;
    }
    MiniPyByError = PyErr_NewException("minipyby.Error", NULL, NULL);
    Py_INCREF(MiniPyByError);
    PyModule_AddObject(module, "Error", MiniPyByError);
    return module;
}