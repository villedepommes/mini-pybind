from distutils.core import setup, Extension

# The first argument of extension is what to use with `import` statements
module1 = Extension('minipyby',
                    sources = ['minipyby.cpp'], language='c++', extra_compile_args=['-std=c++14'])

setup (name = 'minipyby',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])

